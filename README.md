Role Name
=========

The "challenge" playbook installs a basic JBoss Application Server version 7.1.1 and deploy the given WAR file on localhost.

By the end of a successful run of the playbook, the JBoss should be seen running on the ports chosen, on the targeted hosts.

- A HelloWorld application should be available at http://<host>:8080/helloworld
- The challenge application should be available at http://<host>:8080/java-artifact-chef-test

Requirements
------------

- These playbooks were only tested on a Centos 7 hosts with Ansible 2.4 installed.

Role 
--------------

To run the playbook as is, run:

	ansible-playbook -u vagrant -b challenge.yml

If you want it to be deployed on a remote host, edit a "hosts" file and adjust "challenge.yml" accordingly, and run like this:

	ansible-playbook -i hosts -u vagrant -b challenge.yml

Author Information
------------------

Ariel Frozza - arielfrozza@gmail.com



